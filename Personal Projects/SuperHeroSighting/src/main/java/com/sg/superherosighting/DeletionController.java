/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superherosighting;

import com.sg.superherosighting.service.ServiceLayer;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author feng
 */
@Controller
@RequestMapping({"/delete"})
public class DeletionController {
        ServiceLayer service;

    public DeletionController(ServiceLayer service) {
        this.service = service;
    }
    
    @RequestMapping(value="/hero", method=RequestMethod.GET)
    public String deleteHero(HttpServletRequest request){
        int id = Integer.parseInt(request.getParameter("heroId"));

        service.deleteHero(id);
        return "redirect:retrieval/displayAllHeroesPage";
    }
    
    @RequestMapping(value="/power", method=RequestMethod.GET)
    public String deletePower(HttpServletRequest request){
        int id = Integer.parseInt(request.getParameter("powerId"));
        
        service.delPower(id);
        return "redirect:retrieval/displayAllPowersPage";
    }
}
