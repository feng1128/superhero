/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superherosighting;

import com.sg.superherosighting.model.Hero;
import com.sg.superherosighting.model.Organization;
import com.sg.superherosighting.model.Power;
import com.sg.superherosighting.service.ServiceLayer;
import java.util.List;
import org.springframework.stereotype.Controller;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author feng
 */
@Controller
@RequestMapping({"/edit"})
public class EditionController {
        ServiceLayer service;

    public EditionController(ServiceLayer service) {
        this.service = service;
    }
    
    @RequestMapping(value="/displayEditHero", method=RequestMethod.GET)
    public String displayEditHero(Model model){
        Hero hero = new Hero();
        List<Power> powers = service.getAllPowers();
        List<Organization> orgs = service.getAllOrgs();
        model.addAttribute("hero", hero);
        model.addAttribute("powers", powers);
        model.addAttribute("orgs", orgs);
        return "editHero";
    }
    
    @RequestMapping(value="/editHero", method=RequestMethod.POST)
    public String editHero(@ModelAttribute("hero") Hero hero){
        service.updateHero(hero);
        return "redirect:retrieval/displayAllHeroesPage";
    }
}